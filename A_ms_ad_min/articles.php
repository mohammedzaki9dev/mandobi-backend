<?php
require("header/header.php");
require("sidbar/sidbar.php");
$dir=realpath('../index.php');
$file_dir=dirname("$dir");
$path_folder=$file_dir."/all_images/";
?>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Insert</a></li>
			</ul>
<?php
if(isset($_POST['insert_article'])){
    if(empty($_POST['article_name'])){
      $error_m="<p style='color:red;margin-left:40%;font-size:22px;'>من فضلك ضع الاسم  </p>";   
    }else{
        $article_name=security_input($_POST['article_name']);
        $author=security_input($_POST['author']);
        $note_article=security_textarea($_POST['note_article']);
        $text_article=security_textarea($_POST['text_article']);
        /*if(isset($_POST['v_in_slide'])){
            $v_in_slide=1;
        }else{
            $v_in_slide=0;
        }*/
        
       // fuction upload images ///////////------------------>>
        $article_image=upload_image($path_folder,$_FILES['image_rticle']);
        
        $ins_sql="INSERT INTO `articles`( `type`, `cat_id`, `article_name`,`author`, `brief_note`, `article_text`, `article_image`, `date_insert`, `lang`, `is_active`)
                    VALUES ('".$_POST['t']."','','".$article_name."','".$author."','".$note_article."','".$text_article."','".$article_image."',now(),0,1)";
        $ins_res=@mysql_query($ins_sql);
        if($ins_res){
            echo '<div style="color: red;font-size: 24px;text-align: center;">تم الاضافة</div>';
            echo("<meta http-equiv='refresh' content='1; url=articles.php?t=".$_POST['t']."' /> "); 
        }
    }
}

?>
			<?php if(isset($error_m)) echo $error_m;?>
            <br />	
<?php
if(isset($_GET['t'])&& is_numeric($_GET['t'])){
    $t=(int)$_GET['t'];
    if($t>1){
?>		
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>Form</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div style="background-image:url(img/background.jpg) !important;"  class="box-content">
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
						  <fieldset>
                           
							<div class="control-group">
							  <label class="control-label" for="typeahead">Name</label>
							  <div class="controls">
								<input type="text" name="article_name" class="span6 typeahead" id="typeahead"   required="" />
                                <input type="hidden" name="t" value="<?php echo $t; ?>" />
							  </div>
                              </div>
                              <div class="control-group" style="display: none;">
							  <label class="control-label" for="typeahead">Author</label>
							  <div class="controls">
								<input type="text" name="author" class="span6 typeahead" id="typeahead"  />
							  </div>
                              </div>
                              <div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">Brief Notes</label>
							  <div class="controls">
								<textarea style="width:55%;"  id="textarea2" rows="4" name="note_article" required></textarea>
                                
							  </div>
							</div>
                            
							<div class="control-group">
							  <label class="control-label" for="fileInput">Image</label>
							  <div class="controls">
								<input class="input-file uniform_on" id="fileInput" type="file" name="image_rticle"/>
							  </div>
							</div>          
							<div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">details</label>
							  <div class="controls">
								<textarea  id="editor1" rows="3" name="text_article" required></textarea> 
							  </div>
							</div>
                            
							<div class="form-actions">
							  <input type="submit" class="btn btn-primary" name="insert_article"  value="إضافة"/>
							  <button type="reset" class="btn">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->
<?php
}
?>
            <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title >
						<h2><i class="halflings-icon user"></i><span class="break"></span>view</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div style="background-image:url(img/background.jpg) !important;"  class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <!--  <th>Username</th> -->
								  <th>Name</th>
								  <th style="max-width: 20%;">Brief_Note</th>
								  <th>Date</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>

<?php

$articles_view=mysql_query("SELECT * FROM `articles` WHERE `type`='".$t."'");
if(mysql_num_rows($articles_view)>0){
    while($arr_article=mysql_fetch_assoc($articles_view)){
        $article_id=$arr_article['article_id'];
        $article_name=$arr_article['article_name'];
        $author=$arr_article['author'];
        $cat_id=$arr_article['cat_id'];
        $brief_note=htmlspecialchars_decode(stripcslashes($arr_article['brief_note']));
        $article_text=htmlspecialchars_decode(stripcslashes($arr_article['article_text']));
        $article_image=$arr_article['article_image'];
        $date_insert=$arr_article['date_insert'];
        $is_active=$arr_article['is_active'];
        if($article_image=="No image upload"){
            $article_image="No_Photo_Available.jpg";
        }
        if($is_active==0){
            $is_active="InActive";
            $color="#FF0066";
        }else{
            $is_active="&nbsp; Active &nbsp;";
            $color="#00FF00";
        }
?>
				
							<tr>
								<!-- <td style="width: 8% !important;"><?= $publisher; ?></td> href="view_article.php?article=<?=$article_id ; ?>" -->
								<td style="width: 20% !important;" class="center">
                                    <a>
                                    <img src="../all_images/<?=$article_image; ?>" style="width:120px;height:90px"/><br />
                                    <?=$article_name; ?></a>
                                </td>
								<td style="width: 52% !important;text-align: right;" ><?=$brief_note; ?></td>
								<td  style="width: 10% !important;" class="center"><?=$date_insert ; ?></td>
								<td style="padding:30px 0 10px 0;width: 27% !important;">
                                  
                                    <span class="btn btn-info" style="margin-top: 2px;">
                                    <a  class="ask" href="edit_article.php?article=<?=$article_id; ?>&c=0">edit<i class="halflings-icon white edit"></i></a>
                                    </span>
                                    <?php
                                     // values
                                     $table_name="articles"; $url_n="articles.php?t=".$t; $row_name="article_id";  $id_value=$article_id;   
                                    ?>
<?php
if($t>1){
?>
                                    <form action="delete_ms.php" method="POST" style="padding: 1px;float: right;">  
                                        <input type="hidden" name="id" value="<?php echo $id_value ; ?>" />
                                        <input type="hidden" name="table" value="<?php echo $table_name ; ?>" />
                                        <input type="hidden" name="row_table_name" value="<?php echo $row_name ; ?>" />
                                        <input type="hidden" name="url" value="<?php echo $url_n ; ?>" />
                                        <input type="hidden" name="img" value="<?php echo $article_image ; ?>" />
                                        <button type="submit" name="delete" class="btn btn-danger" style="padding: 7px 0;float:right;">delete <i class="halflings-icon white trash"></i></button>
                                    </form>
                                    <!-- END delete button -->
<?php
}
?>
                                    <!--active  button -->
                                    <form action="active_ms.php" method="POST" style="padding: 1px;float: right;">  
                                        <input type="hidden" name="id" value="<?php echo $id_value ; ?>" />
                                        <input type="hidden" name="table" value="<?php echo $table_name ; ?>" />
                                        <input type="hidden" name="row_table_name" value="<?php echo $row_name ; ?>" />
                                        <input type="hidden" name="url" value="<?php echo $url_n ; ?>" />
                                        <button type="submit" name="active_mso" class="btn btn-success" style="background-color:<?=$color;?>;padding: 7px 3px;float: right;"><?=$is_active; ?></button>
                                    </form>
                                    <!--END active  button -->
                                    <!--urgent news  button 
                                    <form action="active_ms_global.php" method="POST" style="padding: 1px;float: right;">  
                                        <input type="hidden" name="id" value="<?php echo $id_value ; ?>" />
                                        <input type="hidden" name="table" value="<?php echo $table_name ; ?>" />
                                        <input type="hidden" name="row_table_name" value="<?php echo $row_name ; ?>" />
                                        <input type="hidden" name="change_row_table" value="is_urgent" />
                                        <input type="hidden" name="url" value="<?php echo $url_n ; ?>" />
                                        <button type="submit" name="active_mso" class="btn btn-success" style="background-color:<?=$color_sl;?>;padding: 7px 3px;float: right;"><?=$view_sl; ?></button>
                                    </form>
                                    <!--END active  button -->
								</td>
							</tr>
<?php
    }
}
?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

<?php
    }
?>    
<?php   
require("footer/footer.php");
?>