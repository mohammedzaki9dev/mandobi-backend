<?php
session_start();
if(isset($_SESSION['username']) && isset($_SESSION['user_pass'])&&$_SESSION['is_active'] !==0){  
header("location:index.php");
exit();
}else{
    require("../ms_con_fig/ms_functions.php");
    require("../ms_con_fig/connect.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
	<!-- start: Meta -->
    	<link rel="shortcut icon" href="img/logo.jpg"/>
	<title>ad_min control</title>
	<meta name="description" content="ad_min control"/>
	<meta name="keyword" content="ad_min control"/>
    <meta name="author" content="mso"/>
	<!-- end: Meta -->
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->	
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	<style type="text/css">
	   body { background: url(img/bg-login.jpg) !important; }
	</style>		
</head>
<body>
<?php
if(isset($_POST['login_admin'])){
    // check if empty       
    if(empty($_POST['admin_name'])&& empty($_POST['pass_admin'])){ 
        $error_m="<strong style='color:red;margin:28%;font-size:11px;'>Input username And password</strong>";
    }elseif(empty($_POST['admin_name'])){ 
        $error1="<strong style='color:red;margin:28%;font-size:11px;'>Input user name</strong>"; 
    }elseif(empty($_POST['pass_admin'])){ 
        $error3="<strong style='color:red;margin:28%;font-size:11px;'>Input password</strong>";
        /*ENd EMPTY*/ 
    }else{
        $admin_name=security_input($_POST['admin_name']);
        $pass_admin=security_input($_POST['pass_admin']);
        $pass_md5_ad=md5($pass_admin);
        $sql="SELECT * FROM `members` where `user_name`='".$admin_name."' and `user_pass`='".$pass_md5_ad."' and `is_active`=1 AND `user_level` >1 ";
        $result=mysql_query($sql,$con) or die (mysql_error()); 
        if($result){
            $num = mysql_num_rows($result); 
            if($num==1){
                $row =mysql_fetch_array($result);
                $id=$row['member_id'];
                $f_name=$row['f_name'];
                $l_name=$row['l_name'];
                $username=$row['user_name'];
                $pass=$row['user_pass'];
                $email=$row['email'];
                $is_active=$row['is_active'];
                $user_level=$row['user_level'];
        ////// وضع اسم المستخدم وكلمة السر فى جلسة ///////////
                $_SESSION['user_id']=$id;
                $_SESSION['full_name']=$f_name." ".$l_name;
                $_SESSION['username']=$username;
                $_SESSION['pass_user']=$pass;
                $_SESSION['email']=$email;
                $_SESSION['is_active']=$is_active;
                $_SESSION['user_level']=$user_level;
?>
<script type="text/javascript">
      window.parent.document.location='index.php';
</script>
      
<?php
            }else{
                $error4= "<strong style='color:red;font-size:11px;'>wrong username or password!!</strong><br/> 
                     <meta http-equiv='refresh' content='1; url=login.php' />";  
            }
        }
     }
  }
?>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="../" target="_blank"><i class="halflings-icon home"></i></a>
					</div>
					<h2>Login to your account</h2><?php if(isset($error_m)) echo $error_m;?>
					<form class="form-horizontal" action="" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i><?php if(isset($error0)) echo $error0;?></span>
								<input class="input-large span10" name="admin_name" id="username" type="text" placeholder="type username" required=""/>
                                
							</div>
							<div class="clearfix"><?php if(isset($error1)) echo $error1;?></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i><?php if(isset($error2)) echo $error2;?></span>
								<input class="input-large span10" name="pass_admin" id="password" type="password" placeholder="type password" required=""/>
							</div>
							<div class="clearfix"><?php if(isset($error3)) echo $error3;?></div>
							
							<div class="button-login">	
								<input type="submit" class="btn btn-primary" name="login_admin" value="Login" />
							</div>
							<div class="clearfix"><?php if(isset($error4)) echo $error4;?></div>
					</form>
						
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>


<?php
}
?>