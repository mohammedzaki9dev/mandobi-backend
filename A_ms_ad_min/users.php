<?php
require("header/header.php");
require("sidbar/sidbar.php");

?>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Members</a></li>
			</ul>
            <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title >
						<h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div style="background-image:url(img/background.jpg) !important;" class="box-content">
<?php
if(isset($_GET['t'])&&is_numeric($_GET['t'])){
    $t=(int)$_GET['t'];
    if(isset($_GET['job_id'])&&is_numeric($_GET['job_id'])){
        $job_id=(int)$_GET['job_id'];
        $search="AND `job_id`='".$job_id."'";
    }else{
       $search=''; 
    }
    if(isset($_GET['active'])&&is_numeric($_GET['active'])){
        $active=(int)$_GET['active'];
        $search.="AND `is_payed`='".$active."'";
    }else{
       $search.=''; 
    }
    if($t==1){
     if($_SESSION['user_level']==5){
        $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=5 ".$search.""); 
        }else{
            $users_view=mysql_query("SELECT *FROM `members` WHERE `member_id`=0 ".$search."");   
        }   
    }elseif($t==2){
       $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=4 ".$search."");  
    }elseif($t==3){
       $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=3 ".$search."");  
    }elseif($t==3){
       $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=3 ".$search."");  
    }elseif($t==4){
       $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=2 ".$search."");  
    }elseif($t==5){
       $users_view=mysql_query("SELECT * FROM `members` WHERE `user_level`=1 ".$search."");  
    }else{
      $users_view=mysql_query("SELECT *FROM `members` WHERE `member_id`<>1 ".$search."");  
    }
if(mysql_num_rows($users_view)<1){
    echo('<div style="color: red;font-size: 24px;text-align: center;">No members</div><br/>');
}else{
?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
<?php
if($t<5){
    if($t<>3){
?>                                  
								  <th>Username</th>
                                  <th>user level</th>
<?php
    }
}

?>
                                  <th>Full Name</th>
                                  <th>Email</th>
								  <th>Telephone</th>
<?php
if($t==4){
?>
                                  <th>mobile</th>
                                  <th>whats app</th>
                                  
								  <th>nationality</th>
                                  <th>voting</th>
<?php
}
?>
								  <th>Date</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>

<?php

    while($arr_user=mysql_fetch_assoc($users_view)){
        $member_id=$arr_user['member_id'];
        $f_name=$arr_user['f_name'];
        $l_name=$arr_user['l_name'];
        $username=$arr_user['user_name'];
        $userpass=$arr_user['user_pass'];
        $email=$arr_user['email'];
        $area_live=$arr_user['place'];
        $tel=$arr_user['tel'];
        $mobile=$arr_user['mobile'];
        //$whatsapp=$arr_user['whatsapp'];
        //$nationality=$arr_user['nationality'];
        $stars_vot=$arr_user['stars_vot'];
        $bad_vot=$arr_user['bad_vot'];
        $member_image=$arr_user['member_image'];
        //$dat_brith=$arr_user['dat_brith'];
        //$prog_id=$arr_user['prog_id'];
        //$qualification=$arr_user['qualification'];
        $date_insert=$arr_user['date_insert'];
        $user_level=$arr_user['user_level'];
        $is_active=$arr_user['is_active'];
        if($is_active==3){
            $is_active="ADMIN";
            $link_act="#";  
        }elseif($is_active==0){
            $is_active="InActive";
            $link_act="active.php?user_id=".$member_id; 
            $color="#FF0066";
        }else{
            $is_active="&nbsp; Active&nbsp;";
            $link_act="active.php?user_id=".$member_id; 
            $color="#00FF00";
        }
        if($member_id==$_SESSION['user_id']){
            $visibility='hidden';
        }else{
            $visibility="";
        }
        
        /*$articles_view=mysql_query("SELECT * FROM `articles` WHERE  `article_id`='".$prog_id."'");
        if(mysql_num_rows($articles_view)>0){
            $arr_article=mysql_fetch_assoc($articles_view);
            $article_id_g=$arr_article['article_id'];
            $article_name=$arr_article['article_name'];
        }else{
            $article_name='';
        }*/
?>
				
							<tr>
<?php
if($t<5){
    if($t<>3){
?>
                                <td style="width: 12% !important;"><a href="permissions.php?user_id=<?=$member_id; ?>"><?=$username; ?></a></td>
                                <td style="width: 8% !important;" class="center"><?=level_user($user_level); ?></td>
<?php
    }
}
?>
                                <td style="width: 17% !important;" class="center"><?=$f_name." ".$l_name; ?></td>
                                <td style="width: 17% !important;text-align: right;" ><?=$email; ?></td>
								<td style="width: 10% !important;padding: 0;" class="center"><?=$mobile; ?></td>
<?php
if($t==4){
?>
                                <td style="width: 8% !important;text-align: right;padding: 0;" ><?=$mobile; ?></td>
                                <td style="width: 15% !important;text-align: right;padding: 0;" ><?=$whatsapp; ?></td>
                                <td style="width: 10% !important;text-align: right;padding: 0;" ><?=$nationality; ?></td>
                                <td style="width: 12% !important;text-align: right;padding: 0;" >
                                 good: <?=$stars_vot; ?>
                                 <br />
                                 bad: <?=$bad_vot; ?>
                                
                                </td>
<?php
}
?>                                
								<td  style="width: 10% !important;" class="center"><?php echo $date_insert; ?></td>
								<td  style="padding:0;width: 25% !important;text-align: center;">
<?php
if($user_level< $_SESSION['user_level']){
?>
									<?php
                                     // values
                                     $table_name="members"; $url_n="users.php?t=".$t; $row_name="member_id";  $id_value=$member_id;   
                                    ?>
									<form action="delete_ms.php" method="POST" style="margin-top: 0px !important;padding: 1px;float: right;height:15px;">  
                                        <input type="hidden" name="id" value="<?php echo $id_value ; ?>" />
                                        <input type="hidden" name="table" value="<?php echo $table_name ; ?>" />
                                        <input type="hidden" name="row_table_name" value="<?php echo $row_name ; ?>" />
                                        <input type="hidden" name="url" value="<?php echo $url_n ; ?>" />
                                        <input type="hidden" name="img" value="<?php echo $member_image ; ?>" />
                                        <button type="submit" name="delete" class="btn btn-danger" style="padding: 7px 0;float:right;">delete <i class="halflings-icon white trash"></i></button>
                                    </form>
                                    <!-- END delete button -->
                                    <!--active  button -->
                                    <form action="active_ms.php" method="POST" style="margin-top: 0px !important;padding: 1px;float: right;height: 15px;">  
                                        <input type="hidden" name="id" value="<?php echo $id_value ; ?>" />
                                        <input type="hidden" name="table" value="<?php echo $table_name ; ?>" />
                                        <input type="hidden" name="row_table_name" value="<?php echo $row_name ; ?>" />
                                        <input type="hidden" name="url" value="<?php echo $url_n ; ?>" />
                                        <button type="submit" name="active_mso" class="btn btn-success" style="background-color:<?=$color;?>;padding: 7px 3px;float: right;"><?=$is_active; ?></button>
                                    </form>
                                    <!--END active  button -->
                                    <span class="btn btn-info" style="float: right;margin-top: 0px;height: 21px; font-size: 14px;padding:7px 0;visibility: <?=$visibility;?>;">
                                    <a  class="ask1" href="edit_user.php?user_id=<?=$member_id; ?>&t=<?php echo $t ; ?>" style="padding: 9.5px;">edit<i class="halflings-icon white edit"></i></a>
                                    </span>
<?php
}
?>
								</td>
							</tr>
<?php
    }
?>
						  </tbody>
					  </table>
<?php
}
?>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

		<div style="min-height: 370px;"></div>
    


					
<?php
}
require("footer/footer.php");
?>