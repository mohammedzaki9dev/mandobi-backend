<?php
@session_start();
if(!isset($_SESSION['username']) || !isset($_SESSION['pass_user'])){  
@header("location:login.php");
exit();
}else{
    $username=$_SESSION['username'];
    
 }
    require("../ms_con_fig/connect.php");
    require("../ms_con_fig/ms_functions.php");
    include('header/users_levels.php');
    //include("../ms_con_fig/site_description.php");
    if($_SESSION['is_active']=="0" || $_SESSION['user_level']<4){
?>
<script type="text/javascript">
      window.parent.document.location='../index.php';
</script>
      
<?php
    }else{

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- start: Meta -->
	<title>ad_min control</title>
    <meta name="description" content="ad_min control"/>
    <meta name="keywords" content="ad_min control"/>
	<meta name="author" content="MSO@Atiafco.com"/>
	
	<link rel="shortcut icon" href="img/logo.png"/>
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
    <link  href="css/ask.css" rel="stylesheet"/>
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet"/>
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link id="base-style" href="css/style.css" rel="stylesheet"/>
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->

	<!-- end: Favicon -->
    <link rel="stylesheet" type="text/css" href="css/ask.css"/>
    <script src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/jconfirmaction.jquery.js"></script>
    <script type="text/javascript">
	
	$(document).ready(function() {
		$('.ask').jConfirmAction();
	});
	
</script>
<script src="js/editor_html/ckeditor.js"></script>
<script src="js/editor_html/adapters/jquery.js"></script>
<script>
$( document ).ready( function() {
	$( 'textarea#editor1' ).ckeditor();
} );
</script>	
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="../index.php" target="_blank"><span>الذهاب للموقع</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
                    <!--
						<li class="dropdown hidden-phone">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white warning-sign"></i>
							</a>
							<ul class="dropdown-menu notifications">
								<li class="dropdown-menu-title">
 									<span>You have 11 notifications</span>
									<a href="#refresh"><i class="icon-repeat"></i></a>
								</li>	
                            	<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New user registration</span>
										<span class="time">1 min</span> 
                                    </a>
                                </li>
							
       
                                <li class="dropdown-menu-sub-footer">
                            		<a>View all notifications</a>
								</li>	
							</ul>
						</li>-->
						<!-- start: Notifications Dropdown
						<li class="dropdown hidden-phone">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white tasks"></i>
							</a>
							<ul class="dropdown-menu tasks">
								<li class="dropdown-menu-title">
 									<span>You have 17 tasks in progress</span>
									<a href="#refresh"><i class="icon-repeat"></i></a>
								</li>
								
                                <li>
                                    <a href="#">
										<span class="header">
											<span class="title">ARM Development</span>
											<span class="percent"></span>
										</span>
                                        <div class="taskProgress progressSlim orange">80</div> 
                                    </a>
                                </li>
								<li>
                            		<a class="dropdown-menu-sub-footer">View all tasks</a>
								</li>	
							</ul>
						</li>
						<!-- end: Notifications Dropdown -->
						<!-- start: Message Dropdown --> 
						<!--<li class="dropdown hidden-phone">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white wrench"></i>English
							</a>
							<ul class="dropdown-menu messages">
								<li class="dropdown-menu-title">
 									<span>Select Language</span>
									
								</li>	
                                  <li class="dropdown-menu-title">
 									<span>Arabic</span>
									<a href="./en/" style="color: lime;"> GO<i class="icon-repeat"></i></a>
								</li>
                               
       	                        <li class="dropdown-menu-title">
 									<span>French</span>
									<a href="./fr/" style="color: lime;"> GO<i class="icon-repeat"></i></a>
								</li>
                            	
							</ul>
						</li>-->
						<!-- end: Message Dropdown -->
						<li>
							<a class="btn" href="#">
								
							</a>
						</li>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?=$username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>اعدادات الحساب</span>
								</li>
								<li><a href="user_profile.php"><i class="halflings-icon user"></i> البروفايل</a></li>
								<li><a href="logoutadmin.php"><i class="halflings-icon off"></i> خروج</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
    <?php
    }
    ?>   