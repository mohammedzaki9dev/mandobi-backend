<?php
require("header/header.php");
require("sidbar/sidbar.php");
$dir=realpath('../index.php');
$file_dir=dirname("$dir");

?>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Articles</a></li>
			</ul>
            <div style="background-image:url(img/background.jpg) !important;"  class="box-content">
<?php
$articles_view=mysql_query("SELECT * FROM `articles` WHERE `article_id`='".$_REQUEST['article']."'");
if(mysql_num_rows($articles_view)==1){
?>
						<table class="table table-bordered table-striped table-condensed">
						  <thead>
							  <tr>
								  <th style="text-align: center;">Publisher</th>
								  <th style="max-width: 20%;text-align: center;">Article Name</th>
								  <th style="text-align: center;">Date</th>
								  <th style="text-align: center;">Actions</th>
							  </tr>
						  </thead>   
						  <tbody>

<?php
    while($arr_article=mysql_fetch_assoc($articles_view)){
        $article_id=$arr_article['article_id'];
        $article_name=$arr_article['article_name'];
        $publisher=$arr_article['publisher'];
        $brief_note=htmlspecialchars_decode(stripcslashes($arr_article['brief_note']));
        $article_text=htmlspecialchars_decode(stripcslashes($arr_article['article_text']));
        $article_image=$arr_article['article_image'];
        $date_insert=$arr_article['date_insert'];
        $is_active=$arr_article['is_active'];
        $view_inslide=$arr_article['view_inslide'];
        if($article_image=="No image upload"){
            $article_image="No_Photo_Available.jpg";
        }
       if($view_inslide==0){
            $view_sl="un_view";
            $color_sl="#FF0066";
        }else{
            $view_sl="v_in_slide";
            $color_sl="#00FF00";
        }
        if($is_active==0){
            $is_active="InActive";
            $color="#FF0066";
        }else{
            $is_active="&nbsp; Active &nbsp;";
            $color="#00FF00";
        }
 
?>
							<tr>
								<td style="width: 15% !important;font-size: 22px;text-align: center;color: #E64491;"><p><br /><?= $publisher; ?></p></td>
                                <td style="width: 33% !important;text-align:center;font-size: 22px;color: #E64491;" ><p><br /><?=$article_name; ?></p></td>
								<td  style="width: 10% !important;font-size: 16px;color: #E64491;" class="center"><p><br /><?=$date_insert; ?></p></td>
								<td style="padding:10px 0 10px 0;width: 28% !important;">
                                   <?php if($article_id>3){ ?>
									<a style="background-color:<?=$color;?>;" class="btn btn-success" href="active_article.php?article=<?=$article_id; ?>"><?=$is_active; ?></a>
                                    <?php } ?>
                                    <span class="btn btn-info">
                                    <a  class="ask" href="edit_article.php?article=<?=$article_id; ?>">edit<i class="halflings-icon white edit"></i></a>
                                    </span>
                                     <?php if($article_id>3){ ?>
									<span class="btn btn-danger">
									<a class="ask" href="delete_article.php?article=<?=$article_id; ?>&& img=<?=$article_image; ?>"><q style="color: white;">delete<i class="halflings-icon white trash"></i></q></a>
                                    </span>
                                    <?php } ?>
								</td>
							</tr>
						  </tbody>
                          <tfoot>
                          <tr>
								<td style="font-size: 18px;"><br /><br/>Brief_Note<br /><br/><br /></td>
								<td style="" class="center" colspan="3"><p style="text-align: right;font-size: 18px;"><?=$brief_note; ?></p></td>

							</tr>
                            <tr>
								<td style="font-size: 18px;"><br /><br/>Text Article</td>
								<td style="" class="center" colspan="3">
                                    <img src="../image_article/<?=$article_image; ?>" style="width:170px;height:150px;float: left;border-radius: 15px 15px 15px 15px;"/>
                                    <p style="float: right; text-align: right;font-size: 18px;"><?=$article_text; ?></p>
                                </td>
							</tr>
                          </tfoot>
      				  </table>
            <div style="min-height: 250px;"></div>
<?php
    }
}else{
    echo('<div style="min-height: 650px;">لا يوجد مقالات منشورة</div>');
}
?>
    </div>
<?php    
require("footer/footer.php");
?>