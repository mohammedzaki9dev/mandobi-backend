<?php
require("header/header.php");
require("sidbar/sidbar.php");

?>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Members</a></li>
			</ul>
            <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title >
						<h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div style="background-image:url(img/background.jpg) !important;" class="box-content">
<?php
$users_view=mysql_query("SELECT * FROM `members` WHERE  `user_level`=1");   
if(mysql_num_rows($users_view)<1){
    echo('<div style="color: red;font-size: 24px;text-align: center;">لايوجد اعضاء</div><br/>');
}else{
?>
						<table  class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th style="text-align: center;">الصورة الشخصية</th>
								  <th style="text-align: center;">رقم الهوية</th>
								  <th style="text-align: center;">الاســـم/الايميل</th>
                                  <th style="text-align: center;">الهاتف</th>
                                  <th style="text-align: center;">محل الاقامة</th>
								  <th style="text-align: center;">الحالة الاجتماعية</th>
								  <th style="text-align: center;">تفعيل كمستخدم</th>
								  <th style="text-align: center;">Actions</th>
							  </tr>
						  </thead>   
						  <tbody>

<?php
    while($arr_user=mysql_fetch_assoc($users_view)){
        $member_id=$arr_user['member_id'];
        $f_name=$arr_user['f_name'];
        $s_name=$arr_user['s_name'];
        $g_name=$arr_user['g_name'];
        $fg_name=$arr_user['fg_name'];
        $gg_name=$arr_user['gg_name'];
        $email=$arr_user['email'];
        $mobile=$arr_user['mobile'];
        $identify_number=$arr_user['identify_number'];
        $age=$arr_user['age'];
        $mem_sx=$arr_user['mem_sx'];
        $area_live=$arr_user['area_live'];
        $marital_status=$arr_user['marital_status'];
        $date_insert=$arr_user['date_insert'];
        $position_id=$arr_user['position_id'];
        $member_image=$arr_user['member_image'];
        $is_active=$arr_user['is_active'];
        if($is_active==0){
            $is_active="InActive";
            $color="#FF0066";
        }else{
            $is_active="&nbsp; Active &nbsp;"; 
            $color="#00FF00";
        }     
?>
				
							<tr>
								<td style="width: 20% !important;font-size: 16px;">
                                
                                    <img border="0" style="height: 100px; width: 150px;border-radius: 0 15px 0 15px;"alt="" src="../all_images/mem_images/<?=$member_image; ?>"/><br/>
                                            
                                </td>
								<td style="width: 10% !important;" class="center"><?=$identify_number; ?></td>
								<td style="width: 15% !important;text-align: right;" >
                                <strong><?=$f_name." ".$s_name." ".$g_name." ".$fg_name." ".$gg_name; ?></strong><br /><br />
                                <?=$email; ?>
                                
                                </td>
                                <td style="width: 10% !important;text-align: right;" ><?=$mobile; ?></td>
                                <td style="width: 12% !important;"><?=$area_live; ?></td>
								<td style="width: 5% !important;" class="center"><?=$marital_status; ?></td>
								<td style="width: 5% !important;text-align: right;" >
                                <span class="btn btn-info" style="margin-top: 3px; height: 21px; border: 1px solid #660000; ">
                                    <a  style="height: 190px; width: 20px; color: #660000;" href="edit_user.php?user_id=<?=$member_id;?>"><i class="icon-user"></i> تفعيل</a>
                                </span>
                                </td>
								<td  style="padding:10px 0;width: 33% !important;text-align: center;">
									<a style="background-color:<?=$color;?>;font-size: 14px;padding:7px 0;" class="btn btn-success" href="active_member.php?member_id=<?=$member_id;?>"><?=$is_active; ?></a>
                                    <span class="btn btn-info" style="font-size: 14px;padding:7px 0;">
                                    <a  class="ask"  href="edit_member.php?member_id=<?=$member_id; ?>">edit<i class="halflings-icon white edit"></i></a>
                                    </span>
									<span class="btn btn-danger" style="font-size: 14px;padding:7px 0;">
									<a class="ask"  href="delete_member.php?member_id=<?=$member_id; ?>&&img=<?=$member_image; ?>"><q style="color: white;">delete<i class="halflings-icon white trash"></i></q></a>
                                    </span>
								</td>
							</tr>
<?php
    }
?>
						  </tbody>
					  </table>
<?php
}
?>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->


    


					
<?php

require("footer/footer.php");
?>