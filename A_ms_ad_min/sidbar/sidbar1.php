
<div class="container-fluid-full">
<div class="row-fluid">
	
<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
	<div class="nav-collapse sidebar-nav">
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li><a href="index.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
            <li>
				<a class="dropmenu" href="#"><i class="icon-group"></i>
                    <span class="hidden-tablet">Members </span>
                    <span style="border-radius: 30px 30px 30px 30px;color: red;"><i class="icon-chevron-down"></i> </span></a>
				<ul>
					<li><a class="submenu"href="users.php"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet"> View Members</span></a></li>
					<li><a class="submenu" href="insert_user.php"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet"> Insert Member</span></a></li>
					
				</ul>	
			</li>
           	<!--<li><a class="submenu" href="art_m.php?t=1"><i class="icon-globe"></i><span class="hidden-tablet">Introduction</span></a></li>-->
           
			<li><a href="messages.php?t=0"><i class="icon-envelope"></i><span class="hidden-tablet"> Messages</span></a></li>
            <li><a href="messages.php?t=1"><i class="icon-envelope"></i><span class="hidden-tablet"> Orders</span></a></li>
            <li><a  href="details.php?d=1"><i class="icon-info-sign"></i><span class="hidden-tablet">Who we are</span></a></li>
            <li><a  href="details.php?d=2"><i class="icon-info-sign"></i><span class="hidden-tablet">What we do</span></a></li>
            <li><a  href="details.php?d=3"><i class="icon-info-sign"></i><span class="hidden-tablet">Visions & values</span></a></li>
            <li><a  href="details.php?d=4"><i class="icon-info-sign"></i><span class="hidden-tablet">Our Standards</span></a></li>
            <li><a  href="details.php?d=5"><i class="icon-info-sign"></i><span class="hidden-tablet">Services</span></a></li>
            <li><a  href="details.php?d=6"><i class="icon-info-sign"></i><span class="hidden-tablet">Brands</span></a></li>
            <li><a  href="details.php?d=7"><i class="icon-info-sign"></i><span class="hidden-tablet">vedio_intro</span></a></li>
            <li><a  href="details.php?d=8"><i class="icon-info-sign"></i><span class="hidden-tablet">Gallary_intro</span></a></li>
            <li><a href="insert_article.php?t=1"><i class="icon-star"></i><span class="hidden-tablet">news</span></a></li>
            <li><a class="submenu" href="slide_imgs.php?v_inhome=0&t=1"><i class="icon-rss"></i><span class="hidden-tablet"> gallary </span></a></li>
            <li><a class="submenu" href="slide_imgs.php?v_inhome=1&t=1"><i class="icon-rss"></i><span class="hidden-tablet"> slider </span></a></li>
            <li><a class="submenu" href="slide_imgs.php?t=2"><i class="icon-rss"></i><span class="hidden-tablet"> WORK team </span></a></li>
            <li><a href="videos.php?t=2"><i class="icon-globe"></i><span class="hidden-tablet">Vedios</span></a></li>
            <li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i>
                    <span class="hidden-tablet">Products </span>
                    <span style="border-radius: 30px 30px 30px 30px;color: red;"><i class="icon-chevron-down"></i> </span></a>
				<ul>
					<li><a class="submenu" href="cat.php?t=3"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">category products</span></a></li>
                    <li><a class="submenu" href="cat_products.php?c=3"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">insert product</span></a></li>
                    <li><a class="submenu" href="view_products.php?t=3"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">view products</span></a></li>
				</ul>	
			</li>
            <li><a href="products_booking.php?t=3"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Products Booking</span></a></li>
            
            <!--<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i>
                    <span class="hidden-tablet">Current projects</span>
                    <span style="border-radius: 30px 30px 30px 30px;color: red;"><i class="icon-chevron-down"></i> </span></a>
				<ul>
					<li><a class="submenu" href="cat.php?t=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">Current projects</span></a></li>
					<li><a class="submenu" href="cat_img.php?c=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">projects images</span></a></li>
					<li><a class="submenu" href="insert_units.php?t=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">projects Unites</span></a></li>
                    <li><a class="submenu" href="view_project_units.php?t=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">view Unites</span></a></li>
                    <li><a href="unit_videos.php?t=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">Uints Vedios</span></a></li>
                    <li><a href="unit_imgs.php?t=2"><i style="color: red;" class="icon-caret-right"></i><span class="hidden-tablet">Uints images</span></a></li>		
				</ul>	
			</li>
            
            <li><a class="submenu" href="insert_article.php?c=2"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">For Customers</span></a></li>
            
            <li><a href="videos.php?t=2"><i class="icon-globe"></i><span class="hidden-tablet">Vedios</span></a></li>
            <li><a class="submenu" href="uploads.php"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> uploads</span></a></li>
            
            
            <li><a class="submenu" href="advertises.php"><i class="icon-rss"></i><span class="hidden-tablet"> Advertising </span></a></li>-->
            <li><a href="details.php"><i class="icon-info-sign"></i><span class="hidden-tablet">Site Details</span></a></li>
		</ul>
	</div>
</div>
<!-- end: Main Menu -->

<noscript>
	<div class="alert alert-block span10">
		<h4 class="alert-heading"></h4>
		<p><a href="https://www.facebook.com/profile.php?id=100003879028763" target="_blank">mso</a> </p>
	</div>
</noscript>
<!-- start: Content -->
<div id="content" class="span10">

