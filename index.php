<?php
@session_start();
require("./ms_con_fig/connect.php");
require("./ms_con_fig/ms_functions.php");
include("./ms_con_fig/site_description.php");
?>
<!DOCTYPE HTML>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title))echo $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php if(isset($description))echo $description; ?>"/>
<meta name="keywords" content="<?php if(isset($kewords))echo $kewords; ?>"/><!doctype html>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="all">
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/evol.colorpicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
<div class="center">
  <h1 class="title">Delegate Rigistration</h1>
  <div class="formbox">
    <div  class="logo"><a href="#"><img src="images/logo.png"></a></div>
    
<?php

$dir=realpath('./index.php');
$file_dir=dirname("$dir");
 $path_folder=$file_dir."/all_images/";
if(isset($_POST['register'])){
    $f_name=security_input($_POST['f_name']);
    $username=security_input($_POST['username']);
    $userpass=security_input($_POST['password']);
    $mobile=security_input($_POST['mobile']);
    $email=security_email($_POST['email']);
    $taxi_type=security_input($_POST['taxi_type']);
    $plate_number=security_input($_POST['plate_number']);
    $color=security_input($_POST['color']);
    
    $place=security_input($_POST['place']);
    $type_of_user=0;//security_input($_POST['type_of_user']);
    if(empty($f_name) ||empty($username) ||empty($userpass) ||empty($email) ||empty($plate_number) ||empty($mobile)){
        echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفوا  أملأ جميع الحقول</div>');
    }else{
        if($userpass<>''&& strlen($userpass)<7){
            echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفوا كلمة السر لابد ان تكون اكبر من 7 احرف</div>'); 
        }elseif($userpass<> trim($_POST['re_pass'])){
            echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفوا لايوجد تطابق بين كلمتى السر</div>'); 
        }else{
            $chk_exist=mysql_query("SELECT * FROM `workers` WHERE  `plate_number`='".$plate_number."'");
            if(mysql_num_rows($chk_exist)>0){
                echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفوا رقم الرخصة موجود من قبل</div>');
            }else{
                $chk_uname_exist=mysql_query("SELECT * FROM `workers` WHERE `user_name`='".$username."'");
                if(mysql_num_rows($chk_uname_exist)>0){
                    echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>من فضلك ضع اسم مستخدم مختلف</div>');  
                }else{
                    $chk_exist1=mysql_query("SELECT * FROM `workers` WHERE  `email`='".$email."'");
                    if(mysql_num_rows($chk_exist1)>0){
                        echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفوا الايميل مسجل من قبل</div>');
                    }else{
                        //get ord for user in this job
                        $count=mysql_query("SELECT count(*) as C FROM `workers` WHERE `user_level`='".$type_of_user."'");
                        if(mysql_num_rows($count)<1){
                            $ord_n=1;
                        }else{
                           $arr_c=mysql_fetch_assoc($count);
                           $ord_n=$arr_c['C']+1; 
                        }               
                        if($_FILES['file']['name']==""){
                            $file='';
                        }else{
                            if($_FILES['file']['size']>(1024*1024*4)){
                               echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>من فضلك قم بتحميل صورة اقل من 4 ميجا</div>');
                               exit; 
                            }else{
                                // fuction upload  ///////////------------------>>
                                $file=upload_image_key($path_folder,$_FILES['file'],"worker");
                            }
                        }
                        ////////////////
                        $user_pass_md5=md5($userpass);
                        //select default value of wallet
                        $q_get_fees_v=mysql_query("SELECT * FROM `details_site` WHERE `detail_id`=8");
                       $arr_f=mysql_fetch_assoc($q_get_fees_v);
                       $fees=(int)$arr_f['detail_text'];
                        $ins_sql="INSERT INTO `workers`( `ord`,`is_connect`, `is_busy`, `user_level`,`my_wallet`, `f_name`, `user_name`, `user_pass`, `email`,  `mobile`,`taxi_type`,`plate_number`,`color`,`member_image`,`place`, `date_insert`, `is_active`,`block`)
                                VALUES ('".$ord_n."',0,0,'".$type_of_user."','".$fees."','".$f_name."','".$username."','".$user_pass_md5."','".$email."','".$mobile."','".$taxi_type."','".$plate_number."','".$color."','".$file."','".$place."',now(),1,0)";
                        $ins_res=mysql_query($ins_sql)or die(mysql_error());
                        if($ins_res==true){
?>
<div class="green-box noneicon eicon"> <i class="fa fa-check"></i>
              <p>تم تسجيل عضويتك بنجاح</p>
            </div>
            <meta http-equiv="refresh" content="2;url=welcome.html" /> 
<?php 
                        exit;
                        }else{
                           echo('<div class="red-box noneicon"><i class="fa fa-exclamation-triangle"></i>عفواهناك خطأ</div>');
                        }
                    }
                } 
            }       
        } 
    }  
}
?>    
    <form method="POST" action="" class="form-inline" role="form" enctype="multipart/form-data">
      
      <div class="form-group">
        <input type="text" name="f_name" placeholder=" First Name" class="form-control">
      </div>
      <!--form-group-->
      <div class="form-group">
        <input type="text" name="username" placeholder="User Name" class="form-control">
      </div>
      <!--form-group-->
      <div class="form-group">
        <input type="password" name="password" placeholder="Password" class="form-control">
      </div>
      <!--form-group-->
      <div class="form-group">
        <input type="password" name="re_pass" placeholder="Re-Password" class="form-control">
      </div>
      
      <!--form-group-->
      <div class="form-group">
        <input type="text" name="mobile"  placeholder="Mobile No" class="form-control">
      </div>
      <!--form-group-->
      
      <div class="form-group">
        <input type="text" name="email"  placeholder="Email" class="form-control">
      </div>
      <!--form-group-->
      
      <div class="form-group">
        <input type="text" name="place" placeholder="Address" class="form-control">
      </div>
      <!--form-group-->
      
      <div class="form-group">
        <div class="rad">
          <div class="bc bc1"></div>
          <label>
            <input type="radio" name="taxi_type" value="1" checked=""/>
            Smail Vehicle</label>
        </div>
        <!--rad-->
        <div class="rad">
          <div class="bc bc2"></div>
          <label>
            <input type="radio"  name="taxi_type" value="2"/>
            Medium Vehicle</label>
        </div>
        <!--rad-->
        <div class="rad">
          <div class="bc bc3"></div>
          <label>
            <input type="radio"  name="taxi_type" value="3"/>
            Large Vehicle</label>
        </div>
        <!--rad--> 
        <i class="border"></i>
      </div>
      <!--form-group-->
      <div class="form-group">
        <input id="transColor1" name="color" class="form-control" placeholder=" vehicle color" value="" />
      </div>
      <!--form-group-->
      
      <div class="form-group">
        <input class="form-control" name="plate_number" placeholder=" Driver’s License No" value="" />
      </div>
      <!--form-group-->
      <div class="form-group">
       <label> Photo:<input type="file"  name="file" placeholder=" Driver’s License No" value="" /></label>
      </div>
      <!--form-group-->
      <div class="form-group">
        <button type="submit" name="register" class="rerister">Register Now</button>
      </div>
      <!--form-group-->
      
    </form>
  </div>
  
  <!--//.formbox-->
  
  <div class="clear"></div>
</div>

<!--//.center-->

</body>
</html>