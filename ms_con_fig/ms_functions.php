<?php
             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////               clean inputs in forms to security         ////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
function security_input($input){
    $input=trim($input);
    $input=strip_tags($input);
    $input=addslashes($input);
    return $input;
}
function security_textarea($input){
    $input=trim($input);
    $input=htmlspecialchars($input);
    $input=addslashes($input);
    return $input;
}
function security_url($url){
    $url=trim($url);
    $url=strip_tags($url);
    $url=htmlspecialchars($url);
    $url=addslashes($url);
    return $url;    
}
function test_input($input_email) {
    $input_email= strip_tags(trim($input_email));
    $input_email= addslashes($input_email);
    return $input_email;
}

function security_email($email) {
    $email= strip_tags(trim($email));
    $email= mysql_real_escape_string($email);
    return $email;
}  
             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////return name with functions strlen AND strrpos AND  substr////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
////////////////////return name with functions strlen AND strrpos AND  substr   ////////////////
function rename_file($name){
    $len= strlen($name);
    $end_dot= strrpos($name,".");
    $ex= substr($name,$end_dot,$len);
    $new_name=time()."_".md5(time()).$ex;
    return $new_name;    
}

             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////                return name with function pathinfo    ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////return name with function pathinfo($name,PATHINFO_EXTENSION); /////////////
function rename2_file($name){
   $extin= pathinfo($name,PATHINFO_EXTENSION); 
   $new_name=time()."_".md5(time()).$extin;
    return $new_name;   
}

             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////               random string with length    ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////return random string and number ; ////////////////////////////////////////////

function randomString($length = 7) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}


             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////                cut_arbic_text    ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////return name with function pathinfo($name,PATHINFO_EXTENSION); /////////////

function cut_arabic_text($text,$length){
   $text = strip_tags($text);
    if (strlen($text) > $length) {
        // truncate string
        $textCut = substr($text, 0, $length);
        // make sure it ends in a word so assassinate doesn't become محمد سعي...
        $cut_text = substr($textCut, 0, strrpos($textCut, ' ')); 
        return $cut_text;
    }else{
        return $text;
    } 
      
}
             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////                       fuction upload images          ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
// fuction upload images ///////////------------------>>
function upload_image($path,$file){
        $img=$file['name'];
        $tmp_name=$file['tmp_name'];
        $type=$file['type'];
        if(is_uploaded_file($tmp_name)){
            //function rename image
            $image=rename_file($img);
            move_uploaded_file($tmp_name,$path.$image)or die("couldn't copy");            
        }else{
            $image="No_Photo_Available.jpg";
        }

   return $image; 
}

 //fuction upload images with keyword /------------------>>
function upload_image_key($path,$file,$word){
        $img=$file['name'];
        $tmp_name=$file['tmp_name'];
        $type=$file['type'];
        if(is_uploaded_file($tmp_name)){
            //function rename image
            $image=$word."_".rename_file($img);
            move_uploaded_file($tmp_name,$path.$image)or die("couldn't copy");            
        }else{
            $image="No_Photo_Available.jpg";
        }

   return $image; 
}

function upload_file($path,$file){
        $old_file_name=$file['name'];
        $tmp_name=$file['tmp_name'];
        $type=$file['type'];
        if(is_uploaded_file($tmp_name)){
            //function rename image
            $new_file_name=rename_file($old_file_name);
            move_uploaded_file($tmp_name,$path.$new_file_name)or die("couldn't copy");            
        }else{
            $new_file_name="No File upload";
        }

   return $new_file_name; 
}

////////////------ END upload images ---------<<



             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////               fuction calculate date post articles   ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////        
// calculate date post articless ///////////------------------>>
function time_publising($time_insert){
           $time=time();
           $time_insert1=strtotime($time_insert);
           $timeall=$time-$time_insert1;
           // ist`s now
           if($timeall ==0){
                $text_view= "الآن";
            //less than minute    
           }elseif($timeall<60){
                $text_view= "قبل اقل من دقيقة";
           //more than minute     
           }elseif($timeall>=60 and $timeall<(60*60)){
                $in=$timeall/60;
                $in=floor($in);
                $text_view="قبل (".$in.") دقيقة";
            //more than hour    
           }elseif($timeall>=(60*60) and $timeall<(60*60*24)){
                $in=$timeall/3600;
                $in=floor($in);
                $text_view="قبل (".$in.") ساعة";
            //more than day    
           }elseif($timeall >= (60*60*24) and $timeall<(60*60*24*7)){
                $in=$timeall/86400;
                $in=floor($in);
                $text_view="قبل (".$in.") ايام";
            //more than weak
           }elseif($timeall >=(60*60*24*7) and $timeall<(60*60*24*30)){
                $in=$timeall/604800;
                $in=floor($in);
                $text_view="قبل (".$in.") اسبوع";
            //more than month
           }elseif($timeall >=(60*60*24*30) and $timeall<(60*60*24*30*12)){
                $in=$timeall/2419200;
                $in=floor($in);
                $text_view="قبل (".$in.") اشهر";
            //min 1 year--> max 3 years 
           }elseif($timeall >=(60*60*24*30*12) and $timeall<(60*60*24*30*12*3)){
                $in=$timeall/29030400;
                $in=floor($in);
                $text_view="قبل (".$in.") سنة";
           //after 3 years
           }elseif($timeall >=(60*60*24*30*12*3)){
                $text_view="أكثر من ثلاث سنوات";
           }
return $text_view;
}
////////////------ END calculate date post articless ---------<<

///////////////---- Get Hejri Time - به مشكلة نهاية اليوم عند الساعة 2 بعد الظهر-------------->>
$time=time(); 
$mydate = new hjridate; 
//echo $mydate->get_hjri("M1-HD1-HM1-CM1-T1",$time); 
class hjridate { 
        var $format; 
        var $time; 
        function get_hjri ($format,$time) { 
            $datelang[1] = "الموافق"; 
            $datelang[2] = "ميلادي"; 
            $datelang[3] = "هجرية"; 
            $datevars=explode ("-",$format); 
            if ( $datevars[0] == "M1" ) { 
                $Christian=1; 
            }else{
                $Christian=0;
            }  
            if ( $datevars[1] == "HD1" ) {
                $hjri_daye_type=1;
            }else{
                $hjri_daye_type = 0;
            } 
            if( $datevars[2]  == "HM1" ){ 
                $hjri_month_type=1;
            }else{
                $hjri_month_type =0;
            } 
            if( $datevars[3] =="CM1" ) { 
                $Christian_month_type=1;
            }else{
                $Christian_month_type =0;
            }  
            if( $datevars[4]=="T1" ){
                $hjri_time= 1;
            }else{
                $hjri_time=0;
            } 
            $TDays=round($time/(60*60*24));  
            $HYear=round($TDays/354.37419);  
            $Remain=$TDays-($HYear*354.37419);  
            $HMonths=round($Remain/29.531182);  
            $HDays=$Remain-($HMonths*29.531182);  
            $HYear=$HYear+1389;  
            $HMonths=$HMonths+10;$HDays=$HDays+23;  
            if ($HDays>29.531188 and round($HDays)!=30){
                $HMonths=$HMonths+1;$HDays=Round($HDays-29.531182);
            }else{  
                $HDays=Round($HDays);  
            }  
            if ($HMonths>12) {  
                $HMonths=$HMonths-12;  
                $HYear = $HYear+1;  
            } 
            $hjre_day = $this->hjri_day ($time,$hjri_daye_type); 
            $hjre_month = $this->hjri_month ($HMonths,$hjri_month_type); 
            $datetime = $hjre_day." : <strong style='color:red'>".$HDays." - ".$hjre_month." - ". $HYear." ".$datelang[3]." </strong>// "; 
            if ( $Christian ) { 
                $dm =  date ("j",$time);
                    $Christian_month = $this->Christian_month ($time,$Christian_month_type); 
                    $Christian_year = date ("Y",$time); 
                    $datetime .=$datelang[1]." : <strong style='color:red'>".$dm." - ".$Christian_month." - ".$Christian_year." ".$datelang[2]."</strong>";  
            } 
            return $datetime; 
        }
        //////////////////////---------------------------
        function hjri_day ($time,$hjri_daye_type){
            if ( $hjri_daye_type == 1 ) {
                $this_day = date ("l",$time); 
                if ( $this_day=="Saturday") $hjri_daye_name="السبت"; 
                if ( $this_day=="Sunday") $hjri_daye_name="الأحد"; 
                if ( $this_day=="Monday") $hjri_daye_name="الاثنين"; 
                if ( $this_day=="Tuesday") $hjri_daye_name="الثلاثاء"; 
                if ( $this_day=="Wednesday") $hjri_daye_name="الأربعاء"; 
                if ( $this_day=="Thursday") $hjri_daye_name="الخميس"; 
                if ( $this_day=="Friday") $hjri_daye_name="الجمعة"; 
            }else{ 
                $hjri_daye_name=""; 
            } 
            return $hjri_daye_name; 
            } 
        function hjri_month ($HMonths,$hejri_month_type) {
            if ($hejri_month_type) {
                if ( $HMonths == 1) $hjri_month_name="محرم"; 
                if ( $HMonths == 2) $hjri_month_name="صفر"; 
                if ( $HMonths == 3) $hjri_month_name="ربيع الأول"; 
                if ( $HMonths == 4) $hjri_month_name="ربيع الثاني"; 
                if ( $HMonths == 5) $hjri_month_name="جمادى الأولى"; 
                if ( $HMonths == 6) $hjri_month_name="جمادى الآخرة"; 
                if ( $HMonths == 7) $hjri_month_name="رجب"; 
                if ( $HMonths == 8) $hjri_month_name="شعبان"; 
                if ( $HMonths == 9) $hjri_month_name="رمضان"; 
                if ( $HMonths == 10) $hjri_month_name="شوال"; 
                if ( $HMonths == 11) $hjri_month_name="ذو القعدة"; 
                if ( $HMonths == 12) $hjri_month_name="ذو الحجة";
            }else{ 
                if ( $HMonths == 1) $hjri_month_name=1; 
                if ( $HMonths == 2) $hjri_month_name=2; 
                if ( $HMonths == 3) $hjri_month_name=3; 
                if ( $HMonths == 4) $hjri_month_name=4; 
                if ( $HMonths == 5) $hjri_month_name=5; 
                if ( $HMonths == 6) $hjri_month_name=6; 
                if ( $HMonths == 7) $hjri_month_name=7; 
                if ( $HMonths == 8) $hjri_month_name=8; 
                if ( $HMonths == 9) $hjri_month_name=9; 
                if ( $HMonths == 10) $hjri_month_name=10; 
                if ( $HMonths == 11) $hjri_month_name=11; 
                if ( $HMonths == 12) $hjri_month_name=12; 
            } 
            return $hjri_month_name; 
        } 
        function Christian_month ($time,$Christian_month_type){ 
                $month_number = date ("m",$time); 
                if ( $Christian_month_type == 1) { 
                        if ($month_number=="1") $Christian_month_name="يناير"; 
                        if ($month_number=="2") $Christian_month_name="فبراير"; 
                        if ($month_number=="3") $Christian_month_name="مارس"; 
                        if ($month_number=="4") $Christian_month_name="أبريل"; 
                        if ($month_number=="5") $Christian_month_name="مايو"; 
                        if ($month_number=="6") $Christian_month_name="يونيو"; 
                        if ($month_number=="7") $Christian_month_name="يوليو"; 
                        if ($month_number=="8") $Christian_month_name="أغسطس"; 
                        if ($month_number=="9") $Christian_month_name="سبتمبر"; 
                        if ($month_number=="10") $Christian_month_name="أكتوبر"; 
                        if ($month_number=="11") $Christian_month_name="نوفمبر"; 
                        if ($month_number=="12") $Christian_month_name="ديسمبر"; 
                }else{ 
                        if ($month_number=="1") $Christian_month_name=1; 
                        if ($month_number=="2") $Christian_month_name=2; 
                        if ($month_number=="3") $Christian_month_name=3; 
                        if ($month_number=="4") $Christian_month_name=4; 
                        if ($month_number=="5") $Christian_month_name=5; 
                        if ($month_number=="6") $Christian_month_name=6; 
                        if ($month_number=="7") $Christian_month_name=7; 
                        if ($month_number=="8") $Christian_month_name=8; 
                        if ($month_number=="9") $Christian_month_name=9; 
                        if ($month_number=="10") $Christian_month_name=10; 
                        if ($month_number=="11") $Christian_month_name=11; 
                        if ($month_number=="12") $Christian_month_name=12; 
                } 
        return $Christian_month_name; 
    } 
} 
/////------------ End Hejri Time --------------<<
            ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////                       fuction esize images           ///////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
           
           
/*function resize_img_uploaded($width, $height){
    // Get original image x y
	list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
	// calculate new image size with ratio 
	$ratio = max($width/$w, $height/$h);
	$h = ceil($height / $ratio);
	$x = ($w - $width / $ratio) / 2;
	$w = ceil($width / $ratio);
	// new file name 
	$path = 'uploads/'.$width.'x'.$height.'_'.$_FILES['image']['name'];
	// read binary data from image file 
	$imgString = file_get_contents($_FILES['image']['tmp_name']);
	// create image from string 
	$image = imagecreatefromstring($imgString);
	$tmp = imagecreatetruecolor($width, $height);
	imagecopyresampled($tmp, $image,0, 0,$x, 0,$width, $height,$w, $h);
	// Save image 
	switch ($_FILES['image']['type']) {
		case 'image/jpeg':
			imagejpeg($tmp, $path, 100);
			break;
		case 'image/png':
			imagepng($tmp, $path, 0);
			break;
		case 'image/gif':
			imagegif($tmp, $path);
			break;
		default:
			exit;
			break;
	}
	return $path;
	//cleanup memory 
	imagedestroy($image);
	imagedestroy($tmp);
}*/
             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////               get date - seconds or minutes or hours    ////////////////
           ////////////////////////////////////////////////////////////////////////////////////////
function dat_seconds($seconds){
    $ful_d=date('Y-m-d  H:i:s');
    $c_d=strtotime($ful_d);
    $min_d=($c_d-$seconds);
    $f_d=date('Y-m-d  H:i:s',$min_d);
    return $f_d;
}
////////////////////--------------------------------------------END ---------------<<
function date_arabic($d){
    $arr_d=explode("-",$d);
    $y=$arr_d[0];
    $m=date_ar_lang($arr_d[1]);
    $dat=$y."  ".$m."  ";
    return $dat;
}
//////////////////////////-------------------------------------END --------------------<<

             ////////////////////////////////////////////////////////////////////////////////////////
            ///////////////               fuction  date time zone  ///////////////////
           //////////////////////////////////////////////////////////////////////////////////////// 
function set_dt_zone_cairo($zon='Africa/Cairo'){
    date_default_timezone_set($zon);
    $d=date('Y-m-d H:i:s', time());
    return $d;
}
function set_dt_zone($zon=date_default_timezone_get){
    date_default_timezone_set($zon);
    $d=date('Y-m-d H:i:s', time());
    return $d;
}
////////////////////---------------------------END -------------------------------<<
///////////////////////////   Convert Currency  ///////////////////////////////

function currencyConverter($currency_from,$currency_to,$currency_input){
    $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
    $yql_query = 'select * from yahoo.finance.xchange where pair in ("'.$currency_from.$currency_to.'")';
    $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
    $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $yql_session = file_get_contents($yql_query_url);
    $yql_json =  json_decode($yql_session,true);
    $currency_output = (float) $currency_input*$yql_json['query']['results']['rate']['Rate'];
    return $currency_output;
}

?>